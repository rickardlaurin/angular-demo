describe('ArticleCtrl', function () {

  var scope, ctrl, httpBackend, url, articles;

  beforeEach(function () {
    module('angular');
    inject(function ($rootScope, $controller, $httpBackend) {
      scope = $rootScope.$new();

      httpBackend = $httpBackend;

      url = 'http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=hpbeliever&api_key=59a34f30f3c5163f936e755463780ad2&format=json';

      articles = {
        recenttracks: {
          track: [
            {
              name: "Sprit"
            }
          ]
        }
      };

      httpBackend
        .whenGET(url)
        .respond(200, articles)

      ctrl = $controller('ArticleCtrl', {$scope: scope});

      httpBackend.flush();
    });
  });

  it('should return an array', function () {
    expect(scope.articles).to.be.an('array');
  });

});