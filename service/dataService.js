angular.module('angular').service('dataService', function ($http) {
  'use strict';

  var baseUrl = 'http://ws.audioscrobbler.com/2.0/'
  ,   apiKey = '59a34f30f3c5163f936e755463780ad2'
  ,   userUrl = baseUrl + '?method=user.getrecenttracks&user=hpbeliever&api_key={apiKey}&format=json'
  ,   artistUrl = baseUrl + '?method=artist.search&artist={artist}&api_key={apiKey}&format=json';

  this.getArticles = function () {
    var promise = $http.get(userUrl.replace('{apiKey}', apiKey)).then(function (result) {
      return result.data.recenttracks.track;
    }, function (err) {
      return err;
    });

    return promise;
  };

  this.searchArticle = function (artist) {
    var promise = $http.get(artistUrl.replace('{apiKey}', apiKey).replace('{artist}', artist)).then(function (result) {
      return result.data.results.artistmatches.artist;
    }, function (err) {
      return err;
    });

    return promise;
  };

});