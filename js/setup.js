angular.module('angular', ['ui.router', 'ngResource']);

angular.module('angular').config(function ($stateProvider, $urlRouterProvider) {
  'use strict';

  $stateProvider.state('article', {
    url: '/',
    templateUrl: 'partial/article/article.html'
  });
	$stateProvider.state('article.details', {
    url: 'details/:name',
    templateUrl: 'partial/article/details/details.html'
  });
	/* Add New Routes Above */
  
  // For any unmatched url, redirect to /
  $urlRouterProvider.otherwise("/");

});

angular.module('angular').run(function ($rootScope) {
  'use strict';

  $rootScope.safeApply = function (fn) {
    var phase = $rootScope.$$phase;
    if (phase === '$apply' || phase === '$digest') {
      if (fn && (typeof(fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };

});