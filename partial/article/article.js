angular.module('angular').controller('ArticleCtrl', function ($scope, dataService) {
  
  'use strict';

  dataService.getArticles().then(function (articles) {
    $scope.articles = articles;
  });

  $scope.search = function () {
    console.log($scope.query);
    dataService.searchArticle($scope.query).then(function (artists) {
      $scope.articles = artists;
    });
  };

});