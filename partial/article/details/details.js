angular.module('angular').controller('ArticleDetailsCtrl', function ($scope, $stateParams) {

  'use strict';

  $scope.article = $scope.articles.filter(function (article) {
    return article.name === $stateParams.name;
  });

});